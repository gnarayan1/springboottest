
CREATE DATABASE `mytest`;

use mytest;


CREATE TABLE `mytest`.`users` (
  `userid` INT AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `firstname` VARCHAR(45),
  `lastname` VARCHAR(45),
  `email` varchar(100),
  PRIMARY KEY (`userid`),
  UNIQUE INDEX `userid_UNIQUE` (`userid` ASC) VISIBLE) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `mytest`.`product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `brand` varchar(45) NOT NULL,
  `madein` varchar(45) NOT NULL,
  `price` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
