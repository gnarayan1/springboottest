package testapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2LoginAuthenticationToken;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationEvents {
	
	Logger logger = LoggerFactory.getLogger(AuthenticationEvents.class);
	
	
    @EventListener
    public void onSuccess(AuthenticationSuccessEvent success) {
    	
    	Authentication authentication = success.getAuthentication();
    	if (authentication instanceof OAuth2LoginAuthenticationToken) {
    		OAuth2LoginAuthenticationToken oauthAuthentication = (OAuth2LoginAuthenticationToken) authentication;
    		logger.trace("Client Name : "+oauthAuthentication.getClientRegistration().getClientName());
    	}
    	  	
    	
    	logger.trace("Logged In Pricipal! : " +authentication.getPrincipal());
    	
    	logger.trace("Granted Authorities! : " +authentication.getAuthorities());
    	
    	logger.trace("Auth Details: " +authentication.getDetails());
    	
    	
    	//Fetch User-Roles Details form our own DB
//    	
//    	Collection<SimpleGrantedAuthority> oldAuthorities = (Collection<SimpleGrantedAuthority>)SecurityContextHolder.getContext().getAuthentication().getAuthorities();
//    	SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_ANOTHER");
//    	List<SimpleGrantedAuthority> updatedAuthorities = new ArrayList<SimpleGrantedAuthority>();
//    	updatedAuthorities.add(authority);
//    	updatedAuthorities.addAll(oldAuthorities);
//    	
//    	SecurityContextHolder.getContext().setAuthentication(
//    	        new UsernamePasswordAuthenticationToken(
//    	                SecurityContextHolder.getContext().getAuthentication().getPrincipal(),
//    	                SecurityContextHolder.getContext().getAuthentication().getCredentials(),
//    	                updatedAuthorities)
//    	);
    	
    }

    @EventListener
    public void onFailure(AbstractAuthenticationFailureEvent failures) {
        // ...
    }
}